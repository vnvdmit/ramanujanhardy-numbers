/*
 ************************************************************************************************
 ************************************************************************************************
 **                                                                                            **
 **                                                                                            **
 **            ##      ## ##     ##  ########     #         #                                  **
 **             ##     ## ##   #### ###     #    ###       ###                                 **
 **              ###   ## ## ### ## ##          ## ##     #   #                                **
 **                ###### ####   ##  ###    #  ##   ##   #######                               **
 **                    ## ##     ##   ####### ##     ## ##     ##                              **
 **                                                                                            **
 **                                                                                            **
 **  #########    #     #### ####     #     ##     ## ###   ###  ######## ##     ##     #      **
 **  ##      ##  ###    ## ### ##    ###    ##     ##  ##  ##   ##     ## ##     ##    ###     **
 **  #########  #   #   ##  #  ##   #   #   #########   ####     ######## #########   #   #    **
 **  ##        #######  ##     ##  #######  ##     ##    ##       ##   ## ##     ##  #######   **
 **  ##       ##     ## ##     ## ##     ## ##     ##   ##      ##     ## ##     ## ##     ##  **
 **                                                                                            **
 **                                                                                            **
 **                                 ##     ##     #     ########     ##### ##     ##           **
 **                                  ##   ##     ###    ##     ##   ##  ## ##   ####           **
 **                                   #####     #   #   ########    ##  ## ## ### ##           **
 **                                  ##   ##   #######  ##        ######## ####   ##           **
 **                                 ##     ## ##     ## ##       ##     ## ##     ##           **
 **                                                                                            **
 **                                                                                            **
 ************************************************************************************************
 ************************************************************************************************
 */

#include <stdio.h>
#include <math.h>
#include "include/RamanujanHardyInt.h"

int main(  ){

	// TODO сделать ввод с клавиатуры

	int iborder;   /* Граница для прекращения выполнения расчётов для производительности*/
	int index = 0; /* Общее количество найденных чисел */
	int n = 1625;  /* Максимальное значение для a, b, c и d */

	unsigned int uNumber;
	unsigned int umi, umk; /* вспомогательные величины для производительности */

	unsigned int uNumbers[MX_NUMBERS];

	unsigned int uArray_of_as[MX_NUMBERS]; /*                                               */
	unsigned int uArray_of_bs[MX_NUMBERS]; /* Куча массивов для                             */
	unsigned int uArray_of_cs[MX_NUMBERS]; /*        хранения значений значений a, b, c и d */
	unsigned int uArray_of_ds[MX_NUMBERS]; /*                                               */

	printf("Processing ");

	for( register int i = 1; i < n; i++ ){

		umi = i*i*i;

		putchar('.');

		for( register int j = i; j < n; j++ ){

			uNumber = umi + j*j*j;

			iborder = ( int )( exp( log( uNumber )/3 ) );
			iborder = ( iborder < n ) ? iborder : n;

			for( register int k = i+1; k < iborder; k++ ){

				umk = k*k*k;

				for( register int q = k; q < iborder; q++ ){

					if ( uNumber == umk + q*q*q ) {

						uNumbers[index] = uNumber;

						uArray_of_as[index] = i;
						uArray_of_bs[index] = j;
						uArray_of_cs[index] = k;
						uArray_of_ds[index] = q;

						index++;

						if ( index == MX_NUMBERS ) { goto END; } /* Если чисел больше, чем надо, то прекратить */

					}

				}

			}

		}

	}

END:printf(" Done!");
    printf("\n\n");
	printf("SORTED:");
	printf("\n\n");

	mycomplexsort( uArray_of_as, uArray_of_bs, uArray_of_cs, uArray_of_ds, uNumbers, index - 1 );

	for( register int i = 0; i <= index - 1; i++ ){

		printf("Index=%4d\ta=%4d\tb=%4d\tc=%4d\td=%4d\tNumber=%10u\n", i + 1, uArray_of_as[i], uArray_of_bs[i],
		                                                                         uArray_of_cs[i], uArray_of_ds[i], uNumbers[i]);

	}
	// TODO: сделать печать в файл
	return 0;
}

void myswap( unsigned int uArray[], int primum, int secundum ){

	unsigned int utmp;

	utmp             = uArray[primum];
	uArray[primum] = uArray[secundum];
	uArray[secundum] =           utmp;

}

void mycomplexsort( unsigned int uArraya[], unsigned int uArrayb[], unsigned int uArrayc[], unsigned int uArrayd[],
                                                                                      unsigned int uSortedArray[], int ilimit ){

	/*
	 * Простая сортировка
	 *              методом вставок
	 *                    совместно нескольких массивов
	 */

	for( register int i = 1; i <= ilimit; i++ ){

		for( register int j = 0; j <= i-1; j++ ){

			if ( uSortedArray[i] < uSortedArray[j] ) {

				     myswap( uArraya, i, j );
				     myswap( uArrayb, i, j );
				     myswap( uArrayc, i, j );
				     myswap( uArrayd, i, j );
				myswap( uSortedArray, i, j );

			}

		}

	}

}
