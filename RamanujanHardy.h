#define MX_NUMBERS 3500 /* Максимальное количество найденных чисел */

void myswap( unsigned int[], int, int ); /* Функция, меняющая два значения в массиве местами */
void mycomplexsort( unsigned int[], unsigned int[], unsigned int[], unsigned int[], unsigned int[], int); /* Функция, 
                                                                     осуществляющая совместную сортировку
                                                                                нескольких массивов */
